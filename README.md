Content inspired in [this Ansible tutorial (Spanish)](https://ualmtorres.github.io/CursoAnsible/tutorial/#trueplaybook-para-inicializar-una-base-de-datos-mysql)

# Tag 1.0

Contains exclusively hosts.cfg and ansible.cfg. It is intended to issue ansible commands from command line with 

    ansible all -m ping
    ansible <role> -a "apt update" --become

# Tag 2.0

Defines the same tasks within playbooks, so that one can issue:

    ansible-playbook apache.yml
    ansible-playbook db.yml

# Tag 3.0

Organize the code in roles (in the roles folder), and playbooks just refer roles:

    ansible-playbook apache-roles.yml
    ansible-playbook db-roles.yml

# Tag 4.0

Uses third party roles from Ansible Galaxy, and reference the roles from php-rules.yml to install a php system.
